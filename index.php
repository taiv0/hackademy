<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Hackademy | Signup form</title>
    <link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="container">
        <h1>Signup</h1>
        <form id="signup-form">
            <label>
                Email:
                <input type="email" name="email" placeholder="Email" data-validate="email"/>
            </label>
            <label>
                Phone:
                <input type="text" name="phone" placeholder="Phone" data-validate="phone"/>
            </label>
            <label>
                Password:
                <input type="password" name="password" placeholder="Password" data-validate="password"/>
            </label>

            <button class="btn btn-primary" type="submit">Submit</button>
        </form>
    </div>

    <script type="text/javascript" src="main.js"></script>
    <script type="text/javascript">
        new Form(document.getElementById('signup-form'));
    </script>
</body>
</html>
