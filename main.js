
const Form = function(formElement) {
    this.fields = formElement.querySelectorAll('[data-validate]'); // find all elements that have data validate attribute
    this.submitBtn = formElement.querySelector('[type="submit"]');
    this.init();
};

Form.prototype.init = function() {
    this.submitBtn.addEventListener('click', (e) => {
        console.log('submit button clicked');

        e.preventDefault();
        if (this.validate()) {
            console.log('Success - validation passed');
        } else {
            console.log('Validation failed');
        }
    });
};
/**
 * Validates all fields
 * @returns {boolean}
 */
Form.prototype.validate = function() {
    let isValid = true;
    this.fields.forEach((input) => {
        console.log('validating ' + input.dataset.validate);

        let validator = 'validate' + input.dataset.validate.replace(/^\w/, c => c.toUpperCase());

        if (false === this[validator].call(this, input)) {
            isValid = false;
        }
    });
    return isValid;
};

/**
 * @param {HTMLInputElement} input
 * @returns {boolean}
 */
Form.prototype.validatePhone = function(input) {
    if (input.value.length < 7 || input.value.length > 8) {
        this.showError(input, 'Invalid phone number');
        return false;
    }
    return true;
};

/**
 * @param {HTMLInputElement} input
 * @returns {boolean}
 */
Form.prototype.validateEmail = function(input) {
    if (!/\S+@\S+\.\S+/.test(input.value)) {
        this.showError(input, 'Invalid email');
        return false;
    }
    return true;
};

/**
 * @param {HTMLInputElement} input
 * @returns {boolean}
 */
Form.prototype.validatePassword = function(input) {
    let minLength = 8;
    if (input.value.length < minLength ) {
        this.showError(input, 'Password length must be at least ' + minLength + ' chars');
        return false;
    }
    if (!/(?=.*[A-Z])/.test(input.value)) {
        this.showError(input, 'Password must contain least one uppercase letter');
        return false;
    }
    if (!/(?=.*[a-z])/.test(input.value)) {
        this.showError(input, 'Password must contain least one lowercase letter');
        return false;
    }
    if (!/(?=.*\d)/.test(input.value)) {
        this.showError(input, 'Password must contain least one number');
        return false;
    }
    return true;
};

/**
 * @param {HTMLInputElement} field
 * @param {String} errorMessage
 */
Form.prototype.showError = function (field, errorMessage) {
    console.log(field.name + ' validation failed: ' + errorMessage);
};



