## Hackademy sample project skeleton with docker

### How to run the app

* Download and install docker
* run ```docker-compose up -d```
* open ```http://localhost:8888``` in your browser
